// src/App.js

import React from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import Header from './components/Header';
import ContestantsPage from './components/ContestantsPage';
import Home from './components/Home';
import Vote from './components/Vote'
// import Home from './pages/Home';
// import About from './pages/About';
// import Services from './pages/Services';
// import Contact from './pages/Contact';

const App = () => {
  return (
    <Router>
      <div className="min-h-screen">
        <Header />
        <Routes>
          <Route path="/" exact element={<Home />} />
          {/* <Route path="/about" component={About} />
          <Route path="/services" component={Services} /> */}
          <Route path="/constestant" element={<ContestantsPage />} />
          <Route path='vote' element={<Vote/>} />
        </Routes>
      </div>
    </Router>
  );
};

export default App;
