// src/components/Contestant.js

import React from 'react';

const Contestant = ({ name, age, image, insta }) => {
  return (
    <div className="bg-white rounded-lg p-4 shadow-md">
      <img src={image} alt={name} className="w-full h-auto rounded-lg" />
      <div className="mt-2">
        <h2 className="text-lg font-semibold">{name}</h2>
        <p className="text-gray-600">Age: {age}</p>
        <a
          href={insta}
          target="_blank"
          rel="noopener noreferrer"
          className="text-blue-500 hover:underline mt-2 block">
          Instagram
        </a>
      </div>
    </div>
  );
};

export default Contestant;
