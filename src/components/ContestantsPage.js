import React from 'react';
import Contestant from './Contestant';
import { contestantList as contestantsData } from '../constestant/constestentList';

const ContestantsPage = () => {
  return (
    <div className="container mx-auto py-8">
      <h1 className="text-3xl font-semibold mb-4">Contestants</h1>
      <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 gap-4">
        {contestantsData.map((contestant, index) => (
          <a key={index} target="_blank" href={contestant.insta}>
            <Contestant {...contestant} />
          </a>
        ))}
      </div>
    </div>
  );
};

export default ContestantsPage;
