import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import logo from '../asserts/logo.png';
import { GoogleAuthProvider, signInWithPopup, signOut } from 'firebase/auth';
import { auth } from '../firebase';
import { useCookies } from 'react-cookie';

const Header = () => {
  const [menuOpen, setMenuOpen] = useState(false);
  const [cookies, setCookie, removeCookie] = useCookies(['user']);
  const [user, setUser] = useState(cookies.user);
  const [showLogout, setShowLogout] = useState(false);
  console.log(user);
  console.log('cu-----', cookies);

  const logout = async () => {
    toggleMenu();
    await signOut(auth);
    setUser(null);

    removeCookie('user');
  };

  const toogleShowLogout = () => setShowLogout(!showLogout);

  const handleLogin = () => {
    signInWithPopup(auth, provider)
      .then((result) => {
        const credential = GoogleAuthProvider.credentialFromResult(result);
        const token = credential.accessToken;
        const user = result.user;
        console.log('u----', user);
        setUser(user);
        setCookie('user', user);
      })
      .catch((error) => {
        // Handle Errors here.
        const errorCode = error.code;
        const errorMessage = error.message;
        // The email of the user's account used.
        const email = error.customData.email;
        // The AuthCredential type that was used.
        const credential = GoogleAuthProvider.credentialFromError(error);
        // ...
      });
  };

  const provider = new GoogleAuthProvider();
  provider.setCustomParameters({
    prompt: 'select_account ',
  });

  const toggleMenu = () => {
    setMenuOpen(!menuOpen);
  };

  return (
    <header className="bg-blue-500 py-4 px-4 md:px-8">
      <div className="flex justify-between items-center">
        <div className="flex items-center">
          <img
            src={logo} // Add your logo image path
            alt="Logo"
            className="w-1/6"
          />
          <Link to="/" className="text-white text-lg font-semibold ml-2">
            Big Boss <span className="font-light text-sm">Season 7</span>
          </Link>
        </div>
        <div className="md:hidden">
          <button
            onClick={toggleMenu}
            className="text-white text-2xl focus:outline-none">
            {menuOpen ? '✕' : '☰'}
          </button>
        </div>
        {/* Desktop navigation */}
        <nav className="hidden md:block">
          <ul className="flex space-x-5" onClick={toggleMenu}>
            <li>
              <Link to="/" className="text-white">
                Home
              </Link>
            </li>
            <li>
              <Link to="/constestant" className="text-white">
                Constestant
              </Link>
            </li>
            <li>
              <Link to="/vote" className="text-white">
                Vote
              </Link>
            </li>
            <li className="w-36">
              {!user ? (
                <Link className="text-white" onClick={handleLogin}>
                  Login
                </Link>
              ) : (
                <div className="w-36 flex-col">
                  <Link className="text-white" onClick={toogleShowLogout}>
                    {user.displayName}
                  </Link>
                  {showLogout && (
                    <button onClick={logout} className="bg-white text-black">
                      Logout
                    </button>
                  )}
                </div>
              )}
            </li>
          </ul>
        </nav>

        {/* Mobile menu */}
        {menuOpen && (
          <div className="md:hidden absolute top-16 left-0 right-0 bg-blue-500 z-10">
            <ul className="text-white text-center py-4">
              <li>
                <Link to="/" className="block py-2" onClick={toggleMenu}>
                  Home
                </Link>
              </li>
              <li>
                <Link
                  to="/constestant"
                  onClick={toggleMenu}
                  className="block py-2">
                  Constestant
                </Link>
              </li>
              <li>
                <Link to="/vote" onClick={toggleMenu} className="block py-2">
                  Vote Now
                </Link>
              </li>
              <li>
                {!user ? (
                  <Link
                    onClick={() => {
                      toggleMenu();
                      handleLogin();
                    }}
                    className="block py-2">
                    Login
                  </Link>
                ) : (
                  <Link className="text-white" onClick={toogleShowLogout}>
                    {user.displayName}
                  </Link>
                )}
              </li>
              {user && (
                <li>
                  <Link onClick={logout} className="block py-2">
                    Logout
                  </Link>
                </li>
              )}
            </ul>
          </div>
        )}
      </div>
    </header>
  );
};

export default Header;
