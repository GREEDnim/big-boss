import React, { useState} from 'react';
import { useCookies } from 'react-cookie';
import { contestantList as contestantsData } from '../constestant/constestentList'; 
import { firestore } from '../firebase';
import { doc, setDoc } from 'firebase/firestore';
export default function Vote(){

    async function addToDB(votes) {
        const db = firestore; 
        const votesDocRef = doc(db, 'votes', 'week1'); 
        try {
            await setDoc(votesDocRef, votes);
            console.log('Votes added to the database successfully');
          } catch (error) {
            console.error('Error adding votes to the database:', error);
          }

    }
    function curVotesSum(votes){
        
        let sum=0;
        console.log(votes)
        for(let ele in votes){
            if(ele) sum+= parseInt(votes[ele])
        }
        console.log(sum)
        return sum;
    }
    const handleVoteChange = (contestantId, newValue) => {

        const potentialRemaining = remaining - newValue + (votes[contestantId] || 0);
        if(potentialRemaining<0) return;
        setRemaining(potentialRemaining)
        setVotes((prevVotes) => ({
          ...prevVotes,
          [contestantId]: newValue,
        }));
      };

      const [cookies] = useCookies(['user']);
      const [votes, setVotes] = useState({});
      const[remaining,setRemaining]=useState(150-curVotesSum(votes));
      const user=cookies.user;
  
     return (
        <>
        {
            !user ? 
            ( 
                <div className="p-10 text-center" >
                    PlEASE LOGIN TO VOTE
                </div>
            ) 
            :
            (
                <>
                <div className='p-10 text-center'>VOTES LEFT: {remaining}</div>
                <div className='grid grid-cols-2 md:grid-cols-3 lg:grid-cols-4 gap-10 p-10'>
                {
                    contestantsData.map((contestant)=>(
                        <div>
                            <img src={contestant.image} alt={contestant.name} className='object-cover h-48 w-48'></img>
                            <p>{contestant.name}</p>
                            <div className='flex gap-2'>
                            <input 
                            type='range' 
                            min='0' max={50} step='5'  
                            value={votes[contestant.id] || 0}
                            onChange={(e) => handleVoteChange(contestant.id, parseInt(e.target.value))} />
                            <p>{votes[contestant.id] || 0}</p>
                            </div>
                            
                        </div>
                    ))
                }
                </div>
                <div className="flex justify-center items-center py-4 ">
                    <button 
                    className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
                    type='submit' onClick={()=>addToDB(votes) }>
                        VOTE
                    </button>
                </div>
                
                </>
                
                
            )
        }
        </>
    )

}

