export const contestantList = [
  {
    id: 1,
    name: 'Raveena Daha',
    age: 19,
    image:
      'https://images.indianexpress.com/2023/10/Raveena-Daha-Instagram-post.jpg',
    insta: 'https://www.instagram.com/im_raveena_daha/',
  },
  {
    id: 2,
    name: 'Ananya Rao',
    age: 25,
    image:
      'https://images.indianexpress.com/2023/10/Nivisha-Instagram-post.jpg',
    insta: 'https://www.instagram.com/_ananyasrao_/',
  },
  {
    id: 3,
    name: 'Maya Krishnan',
    age: 32,
    image:
      'https://images.indianexpress.com/2023/10/Maya-Krishnans-Instagram-post.jpg',
    insta: 'https://www.instagram.com/mayaskrishnan/',
  },
  {
    id: 4,
    name: '​​Jovika Vijaykumar​',
    age: 18,
    image:
      'https://images.indianexpress.com/2023/10/Vanitha-Vijayakumar-with-daughter-in-her-Instagram-post.jpg',
    insta: 'https://www.instagram.com/jovika_vijaykumar/',
  },
  {
    id: 5,
    name: 'Bava Chelladurai',
    age: 61,
    image:
      'https://images.indianexpress.com/2023/10/Writer-Bava-Chelladurai.jpg',
    insta: 'https://www.instagram.com/bavachelladurai/',
  },
  {
    id: 6,
    name: 'Cool Suresh',
    age: 50,
    image:
      'https://images.indianexpress.com/2023/10/Actor-Cool-Sureshs-Instagram-post.jpg',
    insta: 'https://www.instagram.com/actorcoolsureshcool/',
  },
  {
    id: 7,
    name: 'Poornima Ravi',
    age: 28,
    image:
      'https://maplabench.in/wp-content/uploads/2020/05/Poornima-Ravi-1.jpeg',
    insta: 'https://www.instagram.com/poornima_ravii/?hl=en',
  },
  {
    id: 8,
    name: 'Vijay Varma',
    age: 25,
    image:
      'https://static.toiimg.com/thumb/imgsize-66290,msid-104091425,width-700,resizemode-4/104091425.jpg',
    insta: 'https://www.instagram.com/vijayvarma_/?hl=en',
  },
  {
    id: 9,
    name: 'Vichithra',
    age: 49,
    image:
      'https://static.toiimg.com/thumb/imgsize-63806,msid-104091000,width-700,resizemode-4/104091000.jpg',
    insta: 'https://www.instagram.com/vichu_90/',
  },
  {
    id: 10,
    name: 'Yugenran Vasudevan',
    age: 46,
    image:
      'https://static.toiimg.com/thumb/imgsize-128458,msid-104090800,width-700,resizemode-4/104090800.jpg',
    insta: 'https://www.instagram.com/yugendranvasudevan/',
  },
  {
    id: 11,
    name: 'Saravana Vickram',
    age: 28,
    image:
      'https://tamilglitz.in/wp-content/uploads/2023/10/Saravana-Vickram-Bigg-Boss-Tamil-7-Contestant-2.jpg.webp',
    insta: 'https://www.instagram.com/saravanavickram/?hl=en',
  },
  {
    id: 12,
    name: 'Vishnu Vijay',
    age: 35,
    image:
      'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b8/VishnuVijay.png/440px-VishnuVijay.png',
    insta: 'https://www.instagram.com/vishnuvijay_offl/',
  },
  {
    id: 13,
    name: 'Aishu',
    age: 24,
    image: 'https://www.filmibeat.com/img/2023/10/aishuadsmain-1696164270.jpg',
    insta: 'https://www.instagram.com/aishu_ads/?img_index=1',
  },
  {
    id: 14,
    name: 'Mani Chandra',
    age: 29,
    image:
      'https://www.filmibeat.com/fanimg/650x100/fan_images/movie_or_artist_gallery/mani-chandra-dancer-59672-gallery-2.jpg',
    insta: 'https://www.instagram.com/manichandra_official/?hl=en',
  },
  {
    id: 15,
    name: 'Akshaya',
    age: 25,
    image:
      'https://static.toiimg.com/thumb/imgsize-80634,msid-104089952,width-700,resizemode-4/104089952.jpg',
    insta: 'https://www.instagram.com/akshaya__udayakumar/',
  },
  {
    id: 16,
    name: 'Vinusha Devi',
    age: 24,
    image:
      'https://tamilglitz.in/wp-content/uploads/2023/10/Vinusha-Devi-Bigg-Boss-Tamil-7-Contestant-2.jpg.webp',
    insta: 'https://www.instagram.com/vinusha_devi/?hl=en',
  },
  {
    id: 17,
    name: 'Nixen',
    age: 24,
    image:
      'https://tamilglitz.in/wp-content/uploads/2023/10/Nixen-Bigg-Boss-Tamil-7-Contestant-4.jpg.webp',
    insta: 'https://www.instagram.com/nixen_official/',
  },
  {
    id: 18,
    name: 'Pradeep Antony',
    age: 38,
    image:
      'https://tamilglitz.in/wp-content/uploads/2023/10/Pradeep-Antony-Bigg-Boss-Tamil-7-Contestant-5.jpg.webp',
    insta: '',
  },
];
